import express from 'express';
import bodyParser from 'body-parser';
import users from './user/userRouter.mjs';
import admin from './admin/adminRouter.mjs';
import { client } from './utils.mjs';
import * as http from 'http';
import * as fs from 'fs'
import path from 'path'
import { dirname } from 'path';

import {
  createRequire
} from 'module';
const require = createRequire(
  import.meta.url);

// var cors = require('cors');

// const corsOptions = {
//   origin: /upgrad\.dev$/,
//   "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
//   "preflightContinue": false,
//   "optionsSuccessStatus": 204
// }

const app = express();

// app.use(cors(corsOptions));
app.use(bodyParser.json({limit:'50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true }));

http.createServer(function (req, res) {
  res.writeHead(200, { 'content-type': 'text/html' })
  fs.createReadStream('index.html').pipe(res)
}).listen(8080);

app.get('/api',(req,res)=>{
  client.connect(err => {
    if(err) throw err;
    const db = client.db("test")
    db.collection('sample').find({}).toArray((err,result)=>{
      if (err) throw err;
      res.send(result)
    })
    client.close();
  });
})
app.use('/api', users)
app.use('/admin', admin)
app.listen(8000,()=>{
  console.log('Listening on 8000')
})
export default app;