import express from 'express';
import {
  createRequire
} from 'module';
const require = createRequire(
  import.meta.url);

const {
  ObjectId
} = require('mongodb');

import {
  default as dotenv
} from 'dotenv';



import {
  client
} from '../utils.mjs';
var userRouter = express.Router();

let createDBPromise = new Promise((resolve, reject) => {
  client.connect((err) => {
    if (err) {
      reject(err);
    }
    const db = client.db("masterclass")
    resolve(db);
  });
});

createDBPromise.then((db) => {

  userRouter
    .get('/user_health', (req, res) => {
      res.send("Health check success")
    });

  userRouter
    .get('/feed', (req, res) => {
      res.status(200).send({status: "success"});
    });

  userRouter
    .put('/update_progress', async (req, res) => {
      // TODO: Integrate the completed status
      const {
        video_id,
        user_id,
        seek_time
      } = req.body;
      try {
        if (seek_time === 0) {
          await db.collection('feed').findOneAndUpdate({
            _id: new ObjectId(video_id)
          }, {
            $inc: {
              view_count: 1
            }
          }, {
            new: true
          });
        }
        db.collection('users').findOneAndUpdate({
          $and: [{
              user_id: user_id
            },
            {
              progress: {
                $elemMatch: {
                  video_id: video_id
                }
              }
            }
          ]
        }, {
          $set: {
            "progress.$.seek_time": seek_time
          }
        }, {
          new: true
        }, (err, result) => {
          if (err) throw err
          if (!result.value) {
            db.collection('users').findOneAndUpdate({
              user_id: user_id
            }, {
              $push: {
                progress: {
                  'video_id': video_id,
                  'seek_time': seek_time
                }
              }
            }, {
              upsert: true
            }).then((err, result => {
              res.send('success')
            }))
          } else {
            res.send('success')
          }
        })
      } catch (err) {
        console.log(err)
      }
    });

  userRouter
    .get('/categories', (req, res) => {
      res.status(200).send({
        status: "success"
      })
    })

});


export default userRouter;