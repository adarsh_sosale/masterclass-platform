import {
	createRequire
} from 'module';
const require = createRequire(
	import.meta.url);

import express from 'express';
const multer = require('multer')
const upload = multer();

const csvtojson = require('csvtojson');
const fs = require('fs');
const path = require("path");
import { dirname } from 'path';

import checkAuth from "./../checkAuth.mjs"
import {
	default as dotenv
} from 'dotenv';

import {
	client
} from '../utils.mjs';



var adminRouter = express.Router();


let createDBPromise = new Promise((resolve, reject) => {
	client.connect((err) => {
		if (err) {
			reject(err);
		}
		const db = client.db("masterclass")
		resolve(db);
	});
});
createDBPromise.then((db) => {
	adminRouter
		.get('/admin_health', (req, res) => {
			res.send("Health check success")
		});

	adminRouter
		.post('/add_new_video', upload.single('uploadCsv'), async (req, res) => {
			let stringCSV = req.file.buffer.toString();
			console.log(stringCSV);
			let csv_data = await csvtojson().fromString(stringCSV);
			if (csv_data.length > 1) {
				res.status(400).send('Only one entry can be uploaded at a time');
			} else {
				try {
					let sourceData = csv_data[0];
					let orientation = sourceData.is_verical === "1"?"vertical":"horizontal";
					let language = sourceData.is_hindi ? "hi":"en";
					db.collection('feed').insertOne({
						speaker_name: sourceData.speaker_name,
						series_name: sourceData.series_name,
						topic_name: sourceData.topic_name,
						category: sourceData.category.toLowerCase(),
						video_duration: sourceData.video_duration,
						video_description: sourceData.video_description,
						series_description: sourceData.series_description,
						speaker_description: sourceData.speaker_description,
						orientation: orientation,
						language: language,
						thumbnail_link: sourceData.thumbnail_link,
						video_link: sourceData.video_link,
						bc_video_id: Number(sourceData.bc_video_id),
						added_at: Date.now(),
						view_count: 0
					}).then(() => {
						db.collection('categories').findOneAndUpdate({
							category_name: sourceData.category.toLowerCase()
						}, {
							$inc: {
								count: 1
							}
						}, {new: true},(err, result) => {
							if(err) throw err;
							if(result.value) res.send('success');
							else {
								db.collection('categories').insertOne({
									category_name: sourceData.category.toLowerCase(),
									count: 1
								}).then(() => {
									res.send('success')
								})
							}
						})
					})
				} catch(err) {
					console.log(err)
				}
			}
		});

		adminRouter.get("/sample_csv", checkAuth,  (req, res) => {
			const csv_path = path.join(path.resolve(path.dirname('')), 'sample.csv');
			console.log(csv_path)
			fs.readFile('/sample.csv', async (err, data) => {
				if (err) {
				  console.error(err)
				  return
				}
				res.setHeader('Content-disposition', 'attachment; filename=data.csv');
				res.set('Content-Type', 'text/csv');
				res.status(200).send({data});
			  })
		  
		  });

});


export default adminRouter;